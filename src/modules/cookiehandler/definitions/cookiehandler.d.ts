export interface ICookieHandler {
    GetAuthToken(): string | undefined;
    SetAuthToken(token: string): void;
}