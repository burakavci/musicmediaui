import { ICookieHandler } from './definitions/cookiehandler'
import Cookies from 'js-cookie'

class CookieHandler implements ICookieHandler {
    GetAuthToken() : string | undefined{
        return Cookies.get('Authorization');
    }

    SetAuthToken(token: string) : void{
        Cookies.set('Authorization', token);
    }
}

export default CookieHandler;