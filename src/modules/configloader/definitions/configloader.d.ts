type AppConfig = {
    BackendDomain: string,
    LoginRoute: string,
    SocialTestRoute: string,
};