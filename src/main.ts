import 'primevue/resources/themes/saga-blue/theme.css'
import 'primevue/resources/primevue.min.css'
import 'primeicons/primeicons.css'
import 'primeflex/primeflex.css';
import * as Vue from 'vue'
import App from './App.vue'
import Login from "./components/unauthorized/Login.vue";
import Home from "./components/home/Home.vue";
import * as VueRouter from 'vue-router'
import PrimeVue from 'primevue/config'
import Chip from 'primevue/chip';
import Card from 'primevue/card'
import InputText from 'primevue/inputtext'
import Button from 'primevue/button';
import Divider from 'primevue/divider';
import ToastService from 'primevue/toastservice';
import Toast from 'primevue/toast'

const router = VueRouter.createRouter({
    history: VueRouter.createWebHistory('/'),
    routes: [
        {
            name: 'Home', path: '/', component: Home
        }, {
            name: 'Login', path: '/login', component: Login
        },
        {
            name: 'Default', path: '/:pathMatch(.*)*',redirect: {name: 'Home'}
        }
    ]
});

const app = Vue.createApp(App);

app.use(router);
app.use(PrimeVue);
app.use(ToastService);
app.component('Card', Card);
app.component('Chip', Chip)
app.component('Toast', Toast);
app.component('InputText', InputText);
app.component('Button', Button);
app.component('Divider', Divider);
app.mount('#app');
